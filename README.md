# jekyll-liquify

A simple `liquify` filter to parse Liquid templates from a string
variable.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jekyll-liquid'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-liquid

## Usage

Add to your `_config.yml`:

```yaml
plugins:
- jekyll-liquid
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-liquid>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-hardlinks project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.nl/en/code-of-conduct/).

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-liquify-alt'
  spec.version       = '0.1.0'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Adds a filter to render liquid templates from variables'
  spec.description   = 'Parses liquid from variables using a "liquify" filter.  There\'s also jekyll-liquify that\'s exactly the same (sorry! we didn\'t find it at first), but it depends on redcarpet which we don\'t need.'
  spec.homepage      = "https://0xacab.org/sutty/jekyll/#{spec.name}"
  spec.license       = 'GPL-3.0'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.required_ruby_version = '~> 2'

  spec.add_dependency 'jekyll', '~> 4'
end

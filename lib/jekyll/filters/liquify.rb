module Jekyll
  module Filters
    # Parses input and renders it using Liquid
    module Liquify
      def liquify(input)
        unless input == String
          Jekyll.logger.warn "#{input.to_s} can't be parsed as Liquid"
          return ''
        end

        Liquid::Template.parse(input).render(@context)
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Liquify)
